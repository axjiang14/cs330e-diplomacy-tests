#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
#   Write unit tests in TestDiplomacy.py that test corner cases and failure cases 
#   until you have an 3 tests for the function diplomacy_solve(),
#   confirm the expected failures, and add, commit, and push to the private code repo.
#   (https://www.cs.utexas.edu/users/fares/cs330ef19/CS%20373_files/projects/Diplomacy.html)
# -----------


def diplomacy_solve_test_decorator(test_func):
    def diplomacy_solve_test_wrapper(self):
        inputs, outputs = test_func(self)
        inputString = "\n".join(inputs) + "\n"
        outputString = "\n".join(outputs) + "\n"
        r = StringIO(inputString)
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(),outputString)
    return diplomacy_solve_test_wrapper

class TestDiplomacy (TestCase):
    # -----
    # solve
    # -----
    @diplomacy_solve_test_decorator
    def test_solve_1(self):
        inputs = ["A Madrid Hold","B Barcelona Move Madrid","C London Move Madrid","D Paris Support B","E Austin Support A"]
        outputs = ["A [dead]","B [dead]","C [dead]","D Paris","E Austin"]
        return [inputs,outputs]

    @diplomacy_solve_test_decorator
    def test_solve_2(self):
        # changed order of input and removed D's support for B
        inputs = ["E Austin Support A","C London Move Madrid","B Barcelona Move Madrid","A Madrid Hold","D Paris Support E"]
        outputs = ["A Madrid","B [dead]","C [dead]","D Paris","E Austin"]
        return [inputs,outputs]

    @diplomacy_solve_test_decorator
    def test_solve_3(self):
        inputs = ["A Madrid Move Houston","B Boston Move Houston","C London Move Madrid","D Paris Support B","E Austin Support A"]
        outputs = ["A [dead]","B [dead]","C Madrid","D Paris","E Austin"]
        return [inputs,outputs]

    @diplomacy_solve_test_decorator
    def test_solve_4(self):
        inputs = ["A Madrid Move Houston","B Boston Move Houston","C London Move Paris","D Paris Support B","E Austin Support A"]
        outputs = ["A Houston","B [dead]","C [dead]","D [dead]","E Austin"]
        return [inputs,outputs]

# ----
# main
# ----

if __name__ == "__main__":
    main()